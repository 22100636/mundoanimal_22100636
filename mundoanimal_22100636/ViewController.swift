//
//  ViewController.swift
//  mundoanimal_22100636
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Kingfisher
import Alamofire

class Animal: Encodable, Decodable {
    let name: String
    let latin_name: String
    let image_link: String
}

class ViewController: UIViewController {

    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAnimal()
        
    }
    
    func getAnimal(){
        AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self){
            response in
            if let animal = response.value{
                self.imageview.kf.setImage(with: URL(string: animal.image_link))
                self.title = animal.name
                
                
                   }
        }
        }
        
    }




